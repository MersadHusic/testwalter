package h.mersad.testwalter.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import h.mersad.testwalter.model.Photo;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mersad on 9/29/2018.
 */

public class AppController extends Application {

    public API api;

    private static AppController instance;
    private List<Runnable> onUpdatedListeners = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.flickr.com/services/rest/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        api = retrofit.create(API.class);

        Realm.init(getApplicationContext());

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                //.encryptionKey(realmEncryptionKey)
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public static AppController getInstance() {
        return instance;
    }

    public void addOnUpdateListener(Runnable runnable) {
        if (!onUpdatedListeners.contains(runnable))
            onUpdatedListeners.add(runnable);
    }

    public void updateData() {
        api.getImageList().enqueue(new Callback<API.ImageListResponse>() {
            @Override
            public void onResponse(Call<API.ImageListResponse> call, Response<API.ImageListResponse> response) {
                final API.ImageListResponse body = response.body();
                if (body != null)
                    try (Realm realm = Realm.getDefaultInstance()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.where(Photo.class).findAll().deleteAllFromRealm();
                                realm.insertOrUpdate(body.photos.photo);
                            }
                        });

                        for (Runnable onUpdatedListener : onUpdatedListeners)
                            onUpdatedListener.run();
                    }

            }

            @Override
            public void onFailure(Call<API.ImageListResponse> call, Throwable t) {

            }
        });
    }
}
