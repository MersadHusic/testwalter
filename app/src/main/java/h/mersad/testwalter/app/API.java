package h.mersad.testwalter.app;

import java.util.List;

import h.mersad.testwalter.model.Photo;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by Mersad on 9/29/2018.
 */

public interface API {

    class ImageListResponse {
        Photos photos;
        String stat;
    }

    class Photos {
        int page;
        int pages;
        int perPage;
        String total;
        List<Photo> photo;
    }

    @POST("?method=flickr.photos.getRecent&api_key=fc4349f9e71e1d453fc30e425191f868&format=json&nojsoncallback=1")
    Call<ImageListResponse> getImageList();

}
