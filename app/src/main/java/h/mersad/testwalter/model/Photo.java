package h.mersad.testwalter.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Mersad on 9/29/2018.
 */

public class Photo extends RealmObject {

    @PrimaryKey
    public String id;
    public String owner;
    public String secret;
    public String server;
    public int farm;
    public String title;
    public int ispublic;
    public int isfriend;
    public int isfamily;
}
