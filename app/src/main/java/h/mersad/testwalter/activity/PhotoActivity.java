package h.mersad.testwalter.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import h.mersad.testwalter.R;

public class PhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        Glide.with(this).load(getIntent().getStringExtra("link")).into((ImageView) findViewById(R.id.imageView));
    }
}
