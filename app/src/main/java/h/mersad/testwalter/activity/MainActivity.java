package h.mersad.testwalter.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import h.mersad.testwalter.R;
import h.mersad.testwalter.app.AppController;
import h.mersad.testwalter.model.Photo;
import io.realm.Realm;

public class MainActivity extends Activity {
    PhotoAdapter photoAdapter;
    List<Photo> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.photosView);
        photoAdapter = new PhotoAdapter();
        recyclerView.setAdapter(photoAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        AppController.getInstance().addOnUpdateListener(new Runnable() {
            @Override
            public void run() {
                updateViews();
            }
        });
        AppController.getInstance().updateData();

        updateViews();
    }

    public void updateViews() {
        try (Realm realm = Realm.getDefaultInstance()) {
            items = realm.copyFromRealm(realm.where(Photo.class).findAll());
            photoAdapter.notifyDataSetChanged();
        }
    }

    public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

        @Override
        public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
            return new PhotoViewHolder(v);
        }

        @Override
        public void onBindViewHolder(PhotoViewHolder holder, int position) {
            Photo item = items.get(position);

            final String link = "https://farm" + item.farm + ".staticflickr.com/" + item.server + "/" + item.id + "_" + item.secret + ".jpg";

            Glide.with(MainActivity.this).load(link).into(holder.photoView);
            holder.titleView.setText(item.title);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, PhotoActivity.class);
                    intent.putExtra("link", link);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class PhotoViewHolder extends RecyclerView.ViewHolder {

            ImageView photoView;
            TextView titleView;

            PhotoViewHolder(View itemView) {
                super(itemView);

                photoView = itemView.findViewById(R.id.imageView);
                titleView = itemView.findViewById(R.id.imageTitle);
            }
        }
    }
}
